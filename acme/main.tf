#----------
# variables
#----------

variable "base_domain" {}

variable "email_address" {}

variable "hosted_zone" {}

#------------------
# request
#------------------

resource "tls_private_key" "exti_wildcard" {
  algorithm = "RSA"
}

resource "tls_cert_request" "exti_wildcard" {
  key_algorithm   = "RSA"
  private_key_pem = "${tls_private_key.exti_wildcard.private_key_pem}"

  dns_names = ["*.${var.base_domain}", "blockade.${var.base_domain}"]

  subject {
    common_name  = "*.${var.base_domain}"
    organization = "${var.base_domain}"
    country      = "US"
  }
}

#---------------------
# register & validate
#---------------------
resource "acme_registration" "exti_wildcard" {
  account_key_pem = "${tls_private_key.exti_wildcard.private_key_pem}"
  email_address   = "${var.email_address}"
}

resource "acme_certificate" "exti_wildcard" {
  account_key_pem = "${acme_registration.exti_wildcard.account_key_pem}"
  common_name = "*.${var.base_domain}"
  
  dns_challenge {
    provider              = "route53"
    recursive_nameservers = ["8.8.8.8"]
    config {
      AWS_HOSTED_ZONE_ID    = "${var.hosted_zone}"
    }
  }
}

#------------------
# output
#------------------

output "id" {
  value = "${acme_certificate.exti_wildcard.id}"
}

resource "local_file" "key" {
  content = "${acme_certificate.exti_wildcard.private_key_pem}"
  filename = "../${var.base_domain}/${var.base_domain}.key"
}

resource "local_file" "certpem" {
  content = "${acme_certificate.exti_wildcard.certificate_pem}"
  filename = "../${var.base_domain}/${var.base_domain}.pem"
}

resource "local_file" "intermediates" {
  content = "${acme_certificate.exti_wildcard.issuer_pem}"
  filename = "../${var.base_domain}/intermediates.pem"
}

resource "local_file" "chainedp12" {
  content = "${acme_certificate.exti_wildcard.certificate_p12}"
  filename = "../${var.base_domain}/${var.base_domain}.chained.p12"
}
