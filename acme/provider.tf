provider "aws" {}
# just leaving this empty so it inherits from the system. 
# creds in repos? c'mon nah
provider "acme" {
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
  #server_url = "https://acme-staging-v02.api.letsencrypt.org/directory"
}
