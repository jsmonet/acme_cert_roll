# Almost entirely about LE cert rolling

this exists to make new wildcard certs on LE and little else. 

fite me irl

## setup
Get logged into your AWS account. Make sure your key/session/whatever is all set in some way. `~/.aws/credentials`, environmental variables, whatever, will all work fine with the AWS provider block:
```
provider "aws" {}
```

get into the `acme` directory and fill out the vars in `terraform.tfvars`. Next you:

```SHELL
terraform init
terraform plan
terraform apply
```
Assuming you didn't screw up, it'll generate a folder in the reporoot whose name matches the name of your base domain. 

# GLORIOUS VICTORY